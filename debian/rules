#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# Install directory
DESTDIR="$(CURDIR)"/debian/sabnzbdplus

DEB_DIR := $(abspath $(dir $(MAKEFILE_LIST)))
DEB_SRC := $(shell dpkg-parsechangelog -l$(DEB_DIR)/changelog --show-field Source)
DEB_TAG := +dfsg
# clean upstream version
UPS_VER := $(shell dpkg-parsechangelog -l$(DEB_DIR)/changelog --show-field Version | sed 's/.*://; s/-[^-]*$$//; s/\+.*//')
# basedir inside upstream tarball
UPS_DIR := SABnzbd-$(UPS_VER)


%:
	dh $@ --with python3,bash-completion

override_dh_auto_build:
	# Generate translations
	python3 tools/make_mo.py
	find locale -type d -empty -delete

execute_before_dh_clean:
	rm -rf build/ locale/
	find . -wholename "./email/*.tmpl" ! -name "*-en.tmpl" -delete

override_dh_install:
	dh_install \
		--exclude=wizard/README.TXT \
		--exclude=sabnzbd/sabtray.py \
		--exclude=images-split.tar.gz \
		--exclude=/src/

execute_after_dh_install:
	mv -v "$(DESTDIR)"/usr/bin/SABnzbd.py "$(DESTDIR)"/usr/bin/sabnzbdplus
	# Workaround the appstream generator's inability to deal with symlinked icon files
	mkdir -p "$(DESTDIR)"/usr/share/pixmaps
	mv -v "$(DESTDIR)"/usr/share/sabnzbdplus/interfaces/Config/templates/staticcfg/images/logo-arrow.svg "$(DESTDIR)"/usr/share/pixmaps/sabnzbdplus.svg

override_dh_installchangelogs:
	dh_installchangelogs README.mkd


get-orig-source:
	# download, decompress and rename upstream sources
	uscan --noconf --force --verbose --rename --check-dirname-level=0 --destdir=$(CURDIR) --download-version $(UPS_VER) sabnzbdplus
	gunzip $(DEB_SRC)_$(UPS_VER).orig.tar.gz
	mv $(DEB_SRC)_$(UPS_VER).orig.tar $(DEB_SRC)_$(UPS_VER)$(DEB_TAG).orig.tar
	# download the missing source files from cvs
	wget --no-clobber https://salsa.debian.org/python-team/packages/sabnzbdplus/-/archive/missing-sources/sabnzbdplus-missing-sources.tar
	tar -xf sabnzbdplus-missing-sources.tar
	mkdir -p $(CURDIR)/$(UPS_DIR)
	mv $(CURDIR)/sabnzbdplus-missing-sources/missing-sources/* $(CURDIR)/$(UPS_DIR)
	# merge missing sources into upstream tarball and create new orig.tar.gz
	tar --append --owner=root --group=root --mode=a+rX --file $(DEB_SRC)_$(UPS_VER)$(DEB_TAG).orig.tar $(UPS_DIR)
	rm -f $(DEB_SRC)_$(UPS_VER)$(DEB_TAG).orig.tar.gz
	gzip --best $(DEB_SRC)_$(UPS_VER)$(DEB_TAG).orig.tar
	# cleanup leftovers
	rm -rf $(CURDIR)/sabnzbdplus-missing-sources/ sabnzbdplus-missing-sources.tar $(UPS_DIR)
