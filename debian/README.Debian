== Init script ==

On the upstream forums, a very common question is how to set
up the program so it starts automatically on boot. For this
reason an init script is included in the package.

By design nothing is started unless and until a user account
has been set in /etc/default/sabnzbdplus. Having the package
create a user for this very purpose has been considered, but
was decided against due to the disadvantages of such an
approach with regard to:

* permissions issues on downloaded files: this program is
  designed to be an automatic downloader, above all else,
  and does not provide any interface to manage or access
  downloaded files.
* setting things up that way is probably is overly
  complicated for a typical user's needs, and likely to
  increase the demand for support from packager and/or
  upstream developers.

The current init script is therefore geared towards ease of
use for a simple setup, while still fully allowing for the
more advanced options, for example with a dedicated user
account and access to downloaded files regulated through
group membership.


== Interface templates ==

Adding any interface templates that are not available in the
repositories can be done by putting those templates in their
own directory under /usr/share/sabnzbdplus/interfaces/. 
Make sure they follow the standard subdirectory layout:

/usr/share/sabnzbdplus/interfaces/<NAME>/templates/*.tmpl

Templates organised in this way automatically show up as an 
option in the web-based config dropdown menu after a program
restart.
